package com.daugherty.service.domain.student;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class StudentDomainServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(StudentDomainServiceApplication.class, args);
  }
}
