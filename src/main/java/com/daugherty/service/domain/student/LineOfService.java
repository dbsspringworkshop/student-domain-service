package com.daugherty.service.domain.student;

/**
 * @author garethdavies
 *
 */
public enum LineOfService {

  BusinessAnalysis, InformationManagement, ProjectManagement, SoftwareArchitectureAndEngineering

}
