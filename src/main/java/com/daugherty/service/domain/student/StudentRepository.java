package com.daugherty.service.domain.student;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 * @author garethdavies
 */
public interface StudentRepository extends MongoRepository<Student, String> {

    @RestResource(path = "twitterHandle", rel = "student")
    Student findByTwitterHandle(@Param("twitterHandle") String twitterHandle);

    @RestResource(path = "byId", rel = "student")
    Student findById(@Param("id") String id);
}
