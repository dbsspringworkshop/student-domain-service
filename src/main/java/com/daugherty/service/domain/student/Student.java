package com.daugherty.service.domain.student;

import org.springframework.data.annotation.Id;

/**
 * @author garethdavies
 */
public class Student {

    private @Id String id;

    private String name;
    private LineOfService lineOfService;
    private String twitterHandle;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LineOfService getLineOfService() {
        return lineOfService;
    }

    public void setLineOfService(LineOfService lineOfService) {
        this.lineOfService = lineOfService;
    }

    public String getTwitterHandle() {
        return twitterHandle;
    }

    public void setTwitterHandle(String twitterHandle) {
        this.twitterHandle = twitterHandle;
    }

}
