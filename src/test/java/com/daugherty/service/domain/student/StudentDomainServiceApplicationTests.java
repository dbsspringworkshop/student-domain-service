package com.daugherty.service.domain.student;

import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Base64;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class StudentDomainServiceApplicationTests {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private StudentRepository repository;

    // @TestConfiguration
    // class TestMongoConfiguration extends AbstractMongoConfiguration {
    //
    // public static String DATABASE_NAME = "student";
    //
    // @Override
    // protected String getDatabaseName() {
    // return DATABASE_NAME;
    // }
    //
    // @Override
    // public Mongo mongo() throws Exception {
    // return new Fongo("student").getMongo();
    // }
    // }
    //
    // @ClassRule
    // public static InMemoryMongoDb inMemoryMongoDb = newInMemoryMongoDbRule().build();
    //
    // @Rule
    // public MongoDbRule embeddedMongoDbRule =
    // newMongoDbRule().defaultEmbeddedMongoDb(TestMongoConfiguration.DATABASE_NAME);

    @Test
    public void contextLoads() {
        assertThat(repository, is(notNullValue()));
    }

    @Test
    // @UsingDataSet(locations = "StudentDomainServiceApplicationTests.json", loadStrategy = LoadStrategyEnum.INSERT)
    public void getStudentsReturnsAllStudentsInRepository() throws Exception {
        Long initialCount = repository.count();

        MockHttpServletRequestBuilder request = get("/students");

        setStandardHeaders(request);

        mockMvc.perform(request).andExpect(status().isOk())
                .andExpect(content().contentType("application/hal+json;charset=UTF-8"))
                // .andExpect(jsonPath("$._embedded.students", hasSize(4)))
                .andExpect(jsonPath("$._embedded.students", hasSize(initialCount.intValue())))
                .andExpect(jsonPath("$._links.self.href", endsWith("/students")))
                .andExpect(jsonPath("$.page.size", is(20)));
    }

    private static void setStandardHeaders(MockHttpServletRequestBuilder request) {
        HttpHeaders httpHeaders = new HttpHeaders();
        String auth = "user:springworkshop";
        String base64auth = Base64.getEncoder().encodeToString(auth.getBytes());
        String authHeader = "Basic " + base64auth;

        httpHeaders.set("Authorization", authHeader);
        request.headers(httpHeaders);
    }

}
